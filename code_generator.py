import string
import random
import openpyxl

LETTERS = string.ascii_letters
NUMBERS = string.digits  
PUNCTUATION = "!\"#$%&'()*+,-./:;<>?@[\]^_`{|}~"

# print( LETTERS )
# print( NUMBERS )
# print( PUNCTUATION )

def generate_code(length):
    # create alphanumerical from string constants
    printable = f'{LETTERS}{NUMBERS}{PUNCTUATION}'

    # convert printable from string to list and shuffle
    printable = list(printable)
    random.shuffle(printable)

    # generate random password and convert to string
    random_password = random.choices(printable, k=length)
    random_password = ''.join(random_password)
    return random_password

def create_list_of_code(length,size):
    code_list = []
    for i in range(size):
        code = generate_code(length)
        # print("bef:",code)
        code = code + format(i,'02x')
        # print("aft:",code)
        code_list.append(code)
    return code_list

if __name__ == "__main__":
    print(string.punctuation)
    code_list = create_list_of_code(length=8,size=80)
    random.shuffle(code_list)
    offline_list = [code_list[i] for i in range(40)]
    online_list = [code_list[i] for i in range(40,80)]
    print(offline_list)
    print(online_list)

    file_name = 'Offline-Online-Code.xlsx'
    sheet = 'Sheet'

    file = openpyxl.load_workbook(file_name)
    file.active = file[sheet+ '1']
    sheet = file.active

    for i in range(1,41):
        sheet.cell(row=i,column=1).value = offline_list[i-1]

    for j in range(1,41):
        sheet.cell(row=j,column=2).value = online_list[j-1]

    file.save(file_name)


